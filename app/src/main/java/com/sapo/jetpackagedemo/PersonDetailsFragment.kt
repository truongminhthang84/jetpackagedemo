package com.sapo.jetpackagedemo


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels

import androidx.navigation.fragment.navArgs
import com.sapo.jetpackagedemo.databinding.PersonDetailsFragmentBinding

class PersonDetailsFragment : BaseFragment() {

    private lateinit var binding: PersonDetailsFragmentBinding
    private val args: PersonDetailsFragmentArgs by navArgs()

    private val viewModel: PersonDetailsViewModel by viewModels {
        Injector.providePersonDetailsViewModel(args.person)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = PersonDetailsFragmentBinding.inflate(inflater, container, false).apply {
            person = viewModel.person
            lifecycleOwner = viewLifecycleOwner
        }
        context ?: return binding.root
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

}



