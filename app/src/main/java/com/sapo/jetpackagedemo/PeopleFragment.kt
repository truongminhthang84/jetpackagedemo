package com.sapo.jetpackagedemo



import android.os.Bundle
import android.view.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.sapo.jetpackagedemo.databinding.PeopleFragmentBinding

class PeopleFragment : BaseFragment() {

    private lateinit var binding: PeopleFragmentBinding
//    private val args: PeopleFragmentArgs by navArgs()

    private val viewModel: PeopleViewModel by viewModels {
        Injector.providePeopleViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = PeopleFragmentBinding.inflate(inflater, container, false).apply {
            val adapter = PersonAdapter()
            recyclerView.adapter = adapter
            setupUI(adapter, this)
        }
        context ?: return binding.root
        setHasOptionsMenu(true)
        return binding.root
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.onClickMenu()
        return true
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    private fun setupUI(adapter: PersonAdapter, binding: PeopleFragmentBinding){
        viewModel.people.observe(viewLifecycleOwner){
            adapter.submitList(it)
        }
    }

}








