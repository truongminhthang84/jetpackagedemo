package com.sapo.jetpackagedemo

import androidx.lifecycle.*

class PeopleViewModel() : ViewModel() {
    // TODO: Implement the ViewModel
    private var isFiltered: MutableLiveData<Boolean> = MutableLiveData(false)

    val people: LiveData<List<Person>> =  isFiltered.switchMap {
        if (it) {
            MutableLiveData(
                listOf(
                    Person("Ha"),
                    Person("Dung"),
                    Person("Qag")
                )
            )
        } else {
            MutableLiveData(

                listOf(
                    Person("1"),
                    Person("2"),
                    Person("3")
                )
            )
        }
    }


    fun onClickMenu() {
        isFiltered.value = !(isFiltered.value ?: false)

    }
}

class  PeopleViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PeopleViewModel() as T
    }
}

