package com.sapo.jetpackagedemo

import kotlinx.android.parcel.Parcelize
import android.os.Parcelable
@Parcelize
data class Person(var name: String): Parcelable