package com.sapo.jetpackagedemo

object Injector {
    fun providePeopleViewModel(): PeopleViewModelFactory = PeopleViewModelFactory()
}