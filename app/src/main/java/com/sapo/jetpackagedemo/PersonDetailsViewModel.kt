package com.sapo.jetpackagedemo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PersonDetailsViewModel(val person: Person) : ViewModel() {
    // TODO: Implement the ViewModel
}



class  PersonDetailsViewModelFactory(private val person: Person)   : ViewModelProvider.NewInstanceFactory() {


    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PersonDetailsViewModel(person) as T
    }
}



fun Injector.providePersonDetailsViewModel(person: Person): PersonDetailsViewModelFactory = PersonDetailsViewModelFactory(person)
