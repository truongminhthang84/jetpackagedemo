package com.sapo.jetpackagedemo


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sapo.jetpackagedemo.databinding.PersonItemBinding

/**
 * Adapter for the [RecyclerView] in [PeopleFragment].
 */
class PersonAdapter : ListAdapter<Person, RecyclerView.ViewHolder>(PersonDiffCallback()) {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val person = getItem(position)
        (holder as PersonViewHolder).bind(person)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PersonViewHolder(PersonItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false))
    }

    class PersonViewHolder(
        private val binding: PersonItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setOnClickListener {
                binding.person?.let { person ->
                    navigateToPlant(person, it)
                }
            }
        }

        private fun navigateToPlant(
                person: Person,
            it: View
        ) {
            val direction = PeopleFragmentDirections.actionPeopleFragmentToPersonDetailsFragment(person)
            it.findNavController().navigate(direction)
        }

        fun bind(item: Person) {
            binding.apply {
                person = item
                executePendingBindings()
            }
        }
    }
}

private class PersonDiffCallback : DiffUtil.ItemCallback<Person>() {

    override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean {
        return oldItem == newItem
    }
}